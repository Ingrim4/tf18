package uni.leipzig.textmining.util.statistics;

import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class TableStatistics {

	private final String table;
	private final long rowCount;

	private final AtomicInteger threads = new AtomicInteger();

	private final AtomicLong startTime = new AtomicLong(-1);
	private final AtomicLong totalTime = new AtomicLong(-1);

	private final AtomicLong selectCount = new AtomicLong();
	private final AtomicLong sortedCount = new AtomicLong();

	private final AtomicLong lengthFilterCount = new AtomicLong();
	private final AtomicLong repeatFilterCount = new AtomicLong();
	private final AtomicLong startFilterCount = new AtomicLong();
	private final AtomicLong characterFilterCount = new AtomicLong();

	public TableStatistics(String table, long rowCount, int threads) {
		this.table = table;
		this.rowCount = rowCount;
		this.threads.set(threads);
	}

	public String table() {
		return this.table;
	}

	public long rowCount() {
		return this.rowCount;
	}

	public long startTime() {
		return this.startTime.get();
	}

	public long totalTime() {
		return this.totalTime.get();
	}

	public void incrementSelectCount() {
		this.startTime.compareAndSet(-1, System.currentTimeMillis());
		this.selectCount.incrementAndGet();
	}

	public long selectCount() {
		return this.selectCount.get();
	}

	public void incrementSortedCount() {
		this.sortedCount.incrementAndGet();
	}

	public long sortedCount() {
		return this.sortedCount.get();
	}

	public void incrementLengthFilterCount() {
		this.lengthFilterCount.incrementAndGet();
	}

	public long lengthFilterCount() {
		return this.lengthFilterCount.get();
	}

	public void incrementRepeatFilterCount() {
		this.repeatFilterCount.incrementAndGet();
	}

	public long repeatFilterCount() {
		return this.repeatFilterCount.get();
	}

	public void incrementStartFilterCount() {
		this.startFilterCount.incrementAndGet();
	}

	public long startFilterCount() {
		return this.startFilterCount.get();
	}

	public void incrementCharacterFilterCount() {
		this.characterFilterCount.incrementAndGet();
	}

	public long characterFilterCount() {
		return this.characterFilterCount.get();
	}

	public void finish() {
		if (this.threads.decrementAndGet() == 0) {
			this.totalTime.set(System.currentTimeMillis() - this.startTime.get());
		}
	}

	public String format(NumberFormat numberFormat) {
		long excluded = this.lengthFilterCount.get() + this.repeatFilterCount.get() + this.startFilterCount.get()
				+ this.characterFilterCount.get();

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(this.table).append(":\n");

		stringBuilder.append("time: ").append(numberFormat.format(TimeUnit.MILLISECONDS.toSeconds(this.totalTime.get()))).append("s; ");
		stringBuilder.append("sel: ").append(numberFormat.format(this.selectCount.get())).append("; ");
		stringBuilder.append("excl: ").append(numberFormat.format(excluded)).append("; ");
		stringBuilder.append("sort: ").append(numberFormat.format(this.sortedCount.get()));
		stringBuilder.append("\n");

		stringBuilder.append("lengthFilter: ").append(numberFormat.format(this.lengthFilterCount.get())).append("; ");
		stringBuilder.append("repeatFilter: ").append(numberFormat.format(this.repeatFilterCount.get())).append("; ");
		stringBuilder.append("startFilter: ").append(numberFormat.format(this.startFilterCount.get())).append("; ");
		stringBuilder.append("characterFilter: ").append(numberFormat.format(this.characterFilterCount.get()));

		return stringBuilder.toString();
	}
}
