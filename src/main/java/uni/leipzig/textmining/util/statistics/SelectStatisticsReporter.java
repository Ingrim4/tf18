package uni.leipzig.textmining.util.statistics;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SelectStatisticsReporter extends StatisticsReporter {

	private static final Logger LOGGER = LogManager.getLogger();

	private static final NumberFormat NUMBER_FORMAT = NumberFormat.getInstance(Locale.ENGLISH);

	private final List<TableStatistics> statisticsList = new ArrayList<>();
	private final Lock lock = new ReentrantLock();

	private final int threads;

	public SelectStatisticsReporter(int threads) {
		this.threads = threads;
	}

	public void start() {
		for (TableStatistics statistics : this.statisticsList) {
			long rowCount = statistics.rowCount();
			LOGGER.info("{}: rowCount: {} batchSize: {}", statistics.table(), NUMBER_FORMAT.format(rowCount),
					NUMBER_FORMAT.format(rowCount / this.threads));
		}

		LOGGER.info("totalRowCount: {}", NUMBER_FORMAT.format(sum(TableStatistics::rowCount)));

		System.out.println();
		System.out.println();

		super.start();
	}

	public void add(TableStatistics statistics) {
		lock.lock();
		try {
			this.statisticsList.add(statistics);
		} finally {
			lock.unlock();
		}
	}

	private long min(Function<TableStatistics, Long> fieldSelector) {
		long min = Long.MAX_VALUE;
		for (TableStatistics statistics : this.statisticsList) {
			long value = fieldSelector.apply(statistics);
			if (value > 0 && value < min) {
				min = value;
			}
		}
		return min;
	}

	private long sum(Function<TableStatistics, Long> fieldSelector) {
		long sum = 0;
		for (TableStatistics statistics : this.statisticsList) {
			sum += fieldSelector.apply(statistics);
		}
		return sum;
	}

	@Override
	public void run() {
		lock.lock();
		try {

			long startTime = min(TableStatistics::startTime);
			if (startTime < 0) {
				return;
			}

			long selectCount = sum(TableStatistics::selectCount);
			long sortedCount = sum(TableStatistics::sortedCount);

			long lengthFilterCount = sum(TableStatistics::lengthFilterCount);
			long repeatFilterCount = sum(TableStatistics::repeatFilterCount);
			long startFilterCount = sum(TableStatistics::startFilterCount);
			long characterFilterCount = sum(TableStatistics::characterFilterCount);

			long timeSinceStart = System.currentTimeMillis() - startTime;
			long excluded = lengthFilterCount + repeatFilterCount + startFilterCount + characterFilterCount;

			System.out.print("\33[1A\33[2K\33[1A\33[2K");

			LOGGER.info(String.format("time: %ss avg: %.3fmicros sel: %s sort: %s excl: %s",
					NUMBER_FORMAT.format(TimeUnit.MILLISECONDS.toSeconds(timeSinceStart)),
					((float) timeSinceStart / (float) selectCount) * 1000, NUMBER_FORMAT.format(selectCount),
					NUMBER_FORMAT.format(sortedCount), NUMBER_FORMAT.format(excluded)));

			LOGGER.info(String.format("lengthFilter: %s, repeatFilter: %s, startFilter: %s, characterFilter: %s",
					NUMBER_FORMAT.format(lengthFilterCount), NUMBER_FORMAT.format(repeatFilterCount),
					NUMBER_FORMAT.format(startFilterCount), NUMBER_FORMAT.format(characterFilterCount)));
		} finally {
			lock.unlock();
		}
	}

	public void close() {
		this.stopAndWait();

		this.run();

		lock.lock();
		try {
			for (TableStatistics statistics : this.statisticsList) {
				LOGGER.info(statistics.format(NUMBER_FORMAT));
			}

			this.statisticsList.clear();
		} finally {
			lock.unlock();
		}
	}
}
