package uni.leipzig.textmining.util.statistics;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MergeStatisticsReporter extends StatisticsReporter {

	private static final Logger LOGGER = LogManager.getLogger();

	private static final NumberFormat NUMBER_FORMAT = NumberFormat.getInstance(Locale.ENGLISH);

	private final AtomicLong startTime = new AtomicLong(-1);
	private final AtomicLong totalTime = new AtomicLong(-1);

	private final AtomicLong mergeCount = new AtomicLong();
	private final AtomicLong duplicateCount = new AtomicLong();

	public long mergeCount() {
		return this.mergeCount.get();
	}

	public void incrementMergeCount() {
		this.startTime.compareAndSet(-1, System.currentTimeMillis());
		this.mergeCount.incrementAndGet();
	}

	public long duplicateCount() {
		return this.duplicateCount.get();
	}

	public void incrementDuplicateCount() {
		this.duplicateCount.incrementAndGet();
	}

	@Override
	public void run() {
		long startTime = this.startTime.get();
		if (startTime < 0) {
			return;
		}

		long mergeCount = this.mergeCount();
		long duplicateCount = this.duplicateCount();

		long totalTime = this.totalTime.get();
		long timeSinceStart = System.currentTimeMillis() - startTime;

		System.out.print("\33[1A\33[2K");

		LOGGER.info(String.format("time: %ss avg: %.3fmicros merge: %s dups: %s",
				NUMBER_FORMAT.format(TimeUnit.MILLISECONDS.toSeconds(totalTime == -1 ? timeSinceStart : totalTime)),
				((float) timeSinceStart / (float) mergeCount) * 1000, NUMBER_FORMAT.format(mergeCount),
				NUMBER_FORMAT.format(duplicateCount)));
	}

	public void close() {
		this.totalTime.set(System.currentTimeMillis() - this.startTime.get());

		this.stopAndWait();

		this.run();
	}
}
