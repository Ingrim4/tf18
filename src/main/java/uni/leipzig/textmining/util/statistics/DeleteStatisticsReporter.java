package uni.leipzig.textmining.util.statistics;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DeleteStatisticsReporter extends StatisticsReporter {

	private static final Logger LOGGER = LogManager.getLogger();

	private static final NumberFormat NUMBER_FORMAT = NumberFormat.getInstance(Locale.ENGLISH);

	private final AtomicLong startTime = new AtomicLong(-1);
	private final AtomicLong totalTime = new AtomicLong(-1);

	private final AtomicLong deleteCount = new AtomicLong();

	public long deleteCount() {
		return this.deleteCount.get();
	}

	public void incrementDeleteCount() {
		this.startTime.compareAndSet(-1, System.currentTimeMillis());
		this.deleteCount.incrementAndGet();
	}

	@Override
	public void run() {
		long startTime = this.startTime.get();
		if (startTime < 0) {
			return;
		}

		long deleteCount = this.deleteCount();

		long totalTime = this.totalTime.get();
		long timeSinceStart = System.currentTimeMillis() - startTime;

		System.out.print("\33[1A\33[2K");

		LOGGER.info(String.format("time: %ss avg: %.3fmicros del: %s",
				NUMBER_FORMAT.format(TimeUnit.MILLISECONDS.toSeconds(totalTime == -1 ? timeSinceStart : totalTime)),
				((float) timeSinceStart / (float) deleteCount) * 1000, NUMBER_FORMAT.format(deleteCount)));
	}

	public void close() {
		this.totalTime.set(System.currentTimeMillis() - this.startTime.get());

		this.stopAndWait();

		this.run();
	}
}
