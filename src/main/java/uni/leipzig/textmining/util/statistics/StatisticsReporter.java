package uni.leipzig.textmining.util.statistics;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import uni.leipzig.textmining.util.SimpleThreadFactory;

public abstract class StatisticsReporter implements Runnable {

	private final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1,
			new SimpleThreadFactory("performance-reporter", true));
	private ScheduledFuture<?> task;

	public void start() {
		this.task = executorService.scheduleAtFixedRate(this, 500L, 500L, TimeUnit.MILLISECONDS);
	}

	protected void stopAndWait() {
		if (!this.task.isDone()) {
			this.task.cancel(false);
			while (!this.task.isDone())
				;
		}
	}
}
