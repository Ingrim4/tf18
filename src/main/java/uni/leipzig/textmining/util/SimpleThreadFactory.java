package uni.leipzig.textmining.util;

import java.util.Objects;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class SimpleThreadFactory implements ThreadFactory {

	private final AtomicInteger nextId = new AtomicInteger(1);
	private final String prefix;
	private final boolean daemon;
	private final int priority;
	protected final ThreadGroup threadGroup;

	public SimpleThreadFactory(ThreadGroup threadGroup) {
        this(threadGroup.getName(), threadGroup.isDaemon(), Thread.NORM_PRIORITY, threadGroup);
    }

	public SimpleThreadFactory(String poolName) {
        this(poolName, false, Thread.NORM_PRIORITY);
    }

	public SimpleThreadFactory(String poolName, boolean daemon) {
        this(poolName, daemon, Thread.NORM_PRIORITY);
    }

	public SimpleThreadFactory(String poolName, int priority) {
        this(poolName, false, priority);
    }

	public SimpleThreadFactory(String poolName, boolean daemon, int priority) {
        this(poolName, daemon, priority, System.getSecurityManager() == null ?
                Thread.currentThread().getThreadGroup() : System.getSecurityManager().getThreadGroup());
    }

	public SimpleThreadFactory(String poolName, boolean daemon, int priority, ThreadGroup threadGroup) {
        Objects.requireNonNull(poolName, "poolName");

        if (priority < Thread.MIN_PRIORITY || priority > Thread.MAX_PRIORITY) {
            throw new IllegalArgumentException(
                    "priority: " + priority + " (expected: Thread.MIN_PRIORITY <= priority <= Thread.MAX_PRIORITY)");
        }

        prefix = poolName + '-';
        this.daemon = daemon;
        this.priority = priority;
        this.threadGroup = threadGroup;
    }

	@Override
	public Thread newThread(Runnable runnable) {
		Thread thread = new Thread(this.threadGroup, runnable, this.prefix + this.nextId.getAndIncrement());

		try {
			if (thread.isDaemon() != this.daemon) {
				thread.setDaemon(this.daemon);
			}

			if (thread.getPriority() != this.priority) {
				thread.setPriority(this.priority);
			}
		} catch (Exception e) {
		}

		return thread;
	}

}
