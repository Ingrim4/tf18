package uni.leipzig.textmining.util.task;

public interface TaskProducer<E> {

	/**
	 * Returns new dynamically created runnable based on the task object
	 * @param oldTask previous task if there is any
	 * @param newTask next task
	 * @return new runnable
	 */
	Runnable createTask(E oldTask, E newTask);
}
