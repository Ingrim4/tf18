package uni.leipzig.textmining.util.task;

import java.util.Queue;
import java.util.concurrent.locks.Lock;

public class TaskExecutor<E> implements Runnable {

	private final Lock lock;
	private final Queue<E> taskQueue;
	private final TaskProducer<E> taskProducer;
	private final TaskFinalizer<E> taskFinalizer;

	public TaskExecutor(TaskExecutorPool<E> executorPool) {
		this.lock = executorPool.lock;
		this.taskQueue = executorPool.taskQueue;
		this.taskProducer = executorPool.taskProducer;
		this.taskFinalizer = executorPool.taskFinalizer;
	}

	private E next() {
		this.lock.lock();
		try {
			return this.taskQueue.poll();
		} finally {
			this.lock.unlock();
		}
	}

	@Override
	public void run() {
		E oldTask = null;
		while (true) {
			E newTask = this.next();
			if (newTask == null) {
				break;
			}

			// catch any and all errors so the thread doesn't die
			try {
				this.taskProducer.createTask(oldTask, newTask).run();
			} catch (Exception e) {
				e.printStackTrace();
			}

			oldTask = newTask;
		}

		if (this.taskFinalizer != null) {
			this.taskFinalizer.finalizeTask(oldTask);
		}
	}
}
