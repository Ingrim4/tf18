package uni.leipzig.textmining.util.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.Nullable;

import uni.leipzig.textmining.util.SimpleThreadFactory;

public class TaskExecutorPool<E> extends Thread {

	private static final AtomicInteger POOL_NUMBER = new AtomicInteger();

	private final AtomicBoolean running = new AtomicBoolean(false);

	private final int poolSize;
	private final ThreadFactory threadFactory;

	final Lock lock = new ReentrantLock();
	final Queue<E> taskQueue;
	final TaskProducer<E> taskProducer;
	final TaskFinalizer<E> taskFinalizer;
	private final TaskPoolFinalizer poolFinalizer;

	public TaskExecutorPool(int poolSize, Queue<E> taskQueue, TaskProducer<E> taskProducer,
			@Nullable TaskFinalizer<E> taskFinalizer, @Nullable TaskPoolFinalizer poolFinalizer) {
		this.poolSize = poolSize;
		this.taskQueue = taskQueue;
		this.taskProducer = taskProducer;
		this.taskFinalizer = taskFinalizer;
		this.poolFinalizer = poolFinalizer;

		String poolName = "executor-pool-" + POOL_NUMBER.getAndIncrement();
		this.threadFactory = new SimpleThreadFactory(poolName, true, Thread.NORM_PRIORITY, new ThreadGroup(poolName));

		this.setName(poolName);
		this.setDaemon(true);
	}

	@Override
	public void run() {
		if (!this.running.compareAndSet(false, true)) {
			throw new IllegalStateException("task pool is already running");
		}

		List<Thread> threads = new ArrayList<>();

		int poolSize = Math.min(this.poolSize, this.taskQueue.size());
		for (int i = 0; i < poolSize; i++) {
			Thread thread = this.threadFactory.newThread(new TaskExecutor<>(this));
			threads.add(thread);
			thread.start();
		}

		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		this.running.set(false);

		this.poolFinalizer.finalizePool();
	}
}
