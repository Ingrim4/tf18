package uni.leipzig.textmining.util.task;

public interface TaskFinalizer<E> {

	/**
	 * Gets called when the last task for the current thread completed
	 * @param lastTask last completed task
	 */
	void finalizeTask(E lastTask);
}
