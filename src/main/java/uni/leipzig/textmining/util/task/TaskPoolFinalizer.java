package uni.leipzig.textmining.util.task;

public interface TaskPoolFinalizer {

	/**
	 * Gets called once the task pool completed all tasks
	 */
	void finalizePool();
}
