package uni.leipzig.textmining.util.task;

import java.util.Queue;

public class TaskPoolBuilder<E> {

	private int poolSize = Runtime.getRuntime().availableProcessors();
	private TaskFinalizer<E> taskFinalizer = null;
	private TaskPoolFinalizer poolFinalizer = null;

	public TaskPoolBuilder<E> poolSize(int poolSize) {
		if (poolSize < 1) {
			throw new IllegalArgumentException("poolSize has to be bigger than 1");
		}
		this.poolSize = poolSize;
		return this;
	}

	public TaskPoolBuilder<E> taskFinalizer(TaskFinalizer<E> taskFinalizer) {
		this.taskFinalizer = taskFinalizer;
		return this;
	}

	public TaskPoolBuilder<E> poolFinalizer(TaskPoolFinalizer poolFinalizer) {
		this.poolFinalizer = poolFinalizer;
		return this;
	}

	public void build(Queue<E> taskQueue, TaskProducer<E> taskProducer) {
		new TaskExecutorPool<>(poolSize, taskQueue, taskProducer, taskFinalizer, poolFinalizer).start();
	}
}
