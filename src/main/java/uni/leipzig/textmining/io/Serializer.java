package uni.leipzig.textmining.io;

import java.nio.ByteBuffer;

/**
 * Interface for a serializer implementation
 * 
 * @param <E> the type of elements to read/write
 */
public interface Serializer<E> {

	/**
	 * Returns size per element in bytes
	 * @return size per element in bytes
	 */
	int elementSize();

	/**
	 * Reads an element from the given buffer
	 * @param buffer buffer to read from
	 * @return read element
	 */
	E read(ByteBuffer buffer);

	/**
	 * Writes the given element to the given buffer
	 * @param buffer buffer to write to
	 * @param element element to write
	 */
	void write(ByteBuffer buffer, E element);
}
