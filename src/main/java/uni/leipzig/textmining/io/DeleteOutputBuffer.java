package uni.leipzig.textmining.io;

import java.io.Closeable;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import uni.leipzig.textmining.io.path.DeleteLayer;
import uni.leipzig.textmining.sentence.SentenceIdSerializer;
import uni.leipzig.textmining.sentence.SentenceRow;

/**
 * This buffer is collection of many smaller internal buffers which in turn are
 * based of the {@link FileOutputBuffer}. Every database-id has an associated
 * internal buffer for writes of sentence-ids which are to be deleted.
 * This class is thread-safe.
 */
public class DeleteOutputBuffer implements AutoCloseable {

	private final ReadWriteLock lock = new ReentrantReadWriteLock();
	private final Map<Integer, Buffer> buffers = new HashMap<>();

	private final DeleteLayer deleteLayer;
	private final int size;

	public DeleteOutputBuffer(DeleteLayer deleteLayer, int size) {
		this.deleteLayer = deleteLayer;
		this.size = size;
	}

	private FileOutputBuffer<Integer> createFileBuffer(int databaseId) {
		try {
			FileChannel fileChannel = this.deleteLayer.outputFileChannel(databaseId);
			return new FileOutputBuffer<>(fileChannel, SentenceIdSerializer.INSTANCE, this.size);
		} catch (IOException e) {
			throw new RuntimeException("unable to create delete output buffer", e);
		}
	}

	private Buffer getOrCreateBuffer(int databaseId) {
		this.lock.readLock().lock();
		try {
			Buffer buffer = this.buffers.get(databaseId);
			if (buffer != null) {
				return buffer;
			}
		} finally {
			this.lock.readLock().unlock();
		}

		Buffer buffer = new Buffer(this.createFileBuffer(databaseId));

		this.lock.writeLock().lock();
		try {
			this.buffers.putIfAbsent(databaseId, buffer);
			return this.buffers.get(databaseId);
		} finally {
			this.lock.writeLock().unlock();
		}
	}

	/**
	 * Writes a {@link SentenceRow} to its associated internal delete buffer
	 * @param element row to be written
	 */
	public void offer(SentenceRow element) {
		this.offer(element.databaseId, element.sentenceId);
	}

	/**
	 * Writes a sentence-id to the internal delete buffer of the given database-id
	 * @param databaseId database-id of internal delete buffer
	 * @param sentenceId sentence-id to be written
	 */
	public void offer(int databaseId, int sentenceId) {
		this.getOrCreateBuffer(databaseId).offer(sentenceId);
	}

	@Override
	public void close() {
		this.lock.writeLock().lock();
		try {
			for (Buffer buffer : this.buffers.values()) {
				try {
					buffer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			this.buffers.clear();
		} finally {
			this.lock.writeLock().unlock();
		}
	}

	private static class Buffer implements Closeable {

		private final Lock lock = new ReentrantLock();
		private final FileOutputBuffer<Integer> outputBuffer;

		public Buffer(FileOutputBuffer<Integer> outputBuffer) {
			this.outputBuffer = outputBuffer;
		}

		public void offer(int element) {
			this.lock.lock();
			try {
				this.outputBuffer.offer(element);
			} finally {
				this.lock.unlock();
			}
		}

		@Override
		public void close() throws IOException {
			this.lock.lock();
			try {
				this.outputBuffer.close();
			} finally {
				this.lock.unlock();
			}
		}
	}
}
