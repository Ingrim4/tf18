package uni.leipzig.textmining.io;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Collects elements in an internal (binary) buffer and auto flushs the buffer when filled.
 * 
 * @param <E> the type of elements
 */
public class FileOutputBuffer<E> implements Closeable {

	private final FileChannel fileChannel;
	private final Serializer<E> serializer;

	private final ByteBuffer buffer;
	private final int sizePerElement;

	public FileOutputBuffer(FileChannel fileChannel, Serializer<E> serializer, int size) {
		this.fileChannel = fileChannel;
		this.serializer = serializer;

		this.sizePerElement = serializer.elementSize();
		this.buffer = ByteBuffer.allocateDirect(size * sizePerElement);
	}

	/**
	 * Writes the internal buffer to disk and resets it
	 */
	public void writeNextChunk() {
		try {
			// reset buffer position and limit to the amount of data contained in the buffer
			this.buffer.limit(this.buffer.capacity() - this.buffer.remaining());
			this.buffer.rewind();

			// write to file
			this.fileChannel.write(this.buffer);

			// reset buffer for next write
			this.buffer.clear();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Writes an element to internal buffer. If the internal buffer is full it will call {@link FileOutputBuffer#writeNextChunk()}
	 * @param element element to write
	 */
	public void offer(E element) {
		int positon = this.buffer.position();
		this.serializer.write(this.buffer, element);
		this.buffer.position(positon + sizePerElement);

		if (this.buffer.remaining() == 0) {
			this.writeNextChunk();
		}
	}

	@Override
	public void close() throws IOException {
		this.writeNextChunk();
		this.fileChannel.close();
	}
}
