package uni.leipzig.textmining.io.path;

import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Represents a layer in a basePath
 */
public class PathLayer {

	private final PathManager pathManager;
	private final int layer;

	PathLayer(PathManager pathManager, int layer) {
		this.pathManager = pathManager;
		this.layer = layer;
	}

	/**
	 * Create this layers directory
	 */
	public void createLayerPath() {
		this.pathManager.createLayerPath(this.layer);
	}

	/**
	 * Returns amount of runs in this layer
	 * @return amount of runs in this layer
	 */
	public int getRunCount() {
		return this.pathManager.getRunCountForLayer(this.layer);
	}

	/**
	 * Returns a read FileChannel for a given run
	 * @param run associated run
	 * @return read FileChannel
	 * @throws IOException If an I/O error occurs
	 */
	public FileChannel getRunFileChannel(int run) throws IOException {
		return this.pathManager.getRunFileChannel(this.layer, run, IOMode.READ);
	}

	/**
	 * Returns the next write FileChannel
	 * @return write FileChannel
	 * @throws IOException
	 */
	public FileChannel nextRunFileChannel() throws IOException {
		int run = this.pathManager.nextRunCountForLayer(this.layer);
		return this.pathManager.getRunFileChannel(this.layer, run, IOMode.WRITE_RUN);
	}

	@Override
	public String toString() {
		return "[pathManager=" + pathManager + ", layer=" + layer + "]";
	}
}
