package uni.leipzig.textmining.io.path;

import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;

enum IOMode {

	/**
	 * Used for reading files
	 */
	READ(StandardOpenOption.READ),
	/**
	 * Used for writing runs
	 */
	WRITE_RUN(StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE),
	/**
	 * Used for writing {@link DeleteOutputBuffer#Buffer}
	 */
	WRITE_DELETE(StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);

	private IOMode(OpenOption...options) {
		this.options = options;
	}

	private final OpenOption[] options;

	public OpenOption[] options() {
		return options;
	}
}
