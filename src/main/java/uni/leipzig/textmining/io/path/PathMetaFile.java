package uni.leipzig.textmining.io.path;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Meta file for a given base path.
 * 
 * <p>
 * Contains information about the amount of layers and associated run count as well as database-id pairs
 * </p>
 */
public class PathMetaFile {

	private final Path path;

	private final List<Integer> runCountPerLayer = new ArrayList<>();
	private final Map<Integer, String> databaseById = new HashMap<>();

	public PathMetaFile(Path path) {
		this.path = path;
	}

	public int getLayerCount() {
		return this.runCountPerLayer.size();
	}

	public int getRunCountForLayer(int layer) {
		return this.runCountPerLayer.get(layer);
	}

	public int nextRunCountForLayer(int layer) {
		int runCount = 0;
		if (layer < this.runCountPerLayer.size()) {
			runCount = this.runCountPerLayer.get(layer) + 1;
			this.runCountPerLayer.set(layer, runCount);
		} else {
			this.runCountPerLayer.add(layer, runCount);
		}
		return runCount;
	}

	public Iterable<Entry<Integer, String>> getDatabaseIterable() {
		return this.databaseById.entrySet();
	}

	public String getDatabaseById(int id) {
		return this.databaseById.get(id);
	}

	public void addDatabase(String database, int id) {
		this.databaseById.put(id, database);
	}

	public void save() {
		try (DataOutputStream output = new DataOutputStream(Files.newOutputStream(this.path))) {
			output.writeInt(this.runCountPerLayer.size());
			for (int runs : this.runCountPerLayer) {
				output.writeInt(runs);
			}

			output.writeInt(this.databaseById.size());
			for (Entry<Integer, String> database : this.databaseById.entrySet()) {
				output.writeInt(database.getKey());
				output.writeUTF(database.getValue());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void load() {
		this.runCountPerLayer.clear();
		this.databaseById.clear();

		try (DataInputStream input = new DataInputStream(Files.newInputStream(this.path))) {
			int length = input.readInt();
			for (int i = 0; i < length; i++) {
				this.runCountPerLayer.add(input.readInt());
			}

			length = input.readInt();
			for (int i = 0; i < length; i++) {
				this.databaseById.put(input.readInt(), input.readUTF());
			}
		} catch (IOException e) {
			throw new RuntimeException("unable to load path metadata file " + path, e);
		}
	}

	@Override
	public String toString() {
		return "[runCountPerLayer=" + runCountPerLayer + ", databaseById=" + databaseById + "]";
	}
}
