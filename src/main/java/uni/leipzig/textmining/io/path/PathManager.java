package uni.leipzig.textmining.io.path;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map.Entry;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import uni.leipzig.textmining.io.DeleteOutputBuffer;
import uni.leipzig.textmining.io.FileInputBuffer;
import uni.leipzig.textmining.io.FileOutputBuffer;
import uni.leipzig.textmining.io.SortedOutputBuffer;

/**
 * <p>Represents a basePath and its associated meta file and layers</p>
 * 
 * Path structure:
 * <pre>
 * runs/
 *   &lt;base path&gt;/ - {@link PathManager}
 *     meta.dat - {@link PathMetaFile}
 *     -1/ - {@link DeleteLayer}
 *         0.dat - {@link DeleteOutputBuffer}
 *         1.dat - {@link DeleteOutputBuffer}
 *      0/ - {@link PathLayer} select
 *         0.dat - {@link SortedOutputBuffer}/{@link FileInputBuffer}
 *         1.dat - {@link SortedOutputBuffer}/{@link FileInputBuffer}
 *      1/ - {@link PathLayer} 1. merge
 *         0.dat - {@link FileOutputBuffer}/{@link FileInputBuffer}
 *         1.dat - {@link FileOutputBuffer}/{@link FileInputBuffer}
 * </pre>
 * 
 * This class is thread-safe in regards to the meta file all its methods
 */
public class PathManager {

	private static final SimpleDateFormat BASE_DIR_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss-SSSZ");
	private static final Path RUN_PATH = Paths.get("runs/");
	private static final String FILE_SUFFIX = ".dat";
	private static final String META_FILE_PATH = "meta.dat";

	/**
	 * Creates a new {@link PathManager} with the current timestamp as basePath
	 * @return new instance of PathManager
	 */
	public static PathManager createNow() {
		return new PathManager(RUN_PATH.resolve(BASE_DIR_FORMAT.format(new Date())), false);
	}

	/**
	 * Creates a new {@link PathManager} with a given basePath and load its meta file
	 * @param base base path for {@link PathManager} instance
	 * @return new instance of PathManager
	 */
	public static PathManager create(String base) {
		Path path = RUN_PATH.resolve(base).normalize();
		if (!path.startsWith(RUN_PATH)) {
			throw new IllegalArgumentException(path + " is not a child path of " + RUN_PATH);
		}
		if (Files.notExists(path)) {
			return null;
		}
		return new PathManager(path, true);
	}

	private final Lock lock = new ReentrantLock();

	private final Path baseDir;
	private final PathMetaFile metaFile;

	private PathManager(Path baseDir, boolean loadMeta) {
		this.baseDir = baseDir;
		this.metaFile = new PathMetaFile(baseDir.resolve(META_FILE_PATH));
		if (loadMeta) {
			this.metaFile.load();
		}
	}

	/**
	 * Returns path for given layer
	 * @param layer associated layer
	 * @return path
	 */
	private Path path(int layer) {
		return this.baseDir.resolve(Integer.toString(layer));
	}

	/**
	 * Returns path for given layer and run
	 * @param layer associated layer
	 * @param run associated run
	 * @return path
	 */
	private Path path(int layer, int run) {
		return path(layer).resolve(run + FILE_SUFFIX);
	}

	/**
	 * Create the directory for given layer
	 * @param layer associated layer
	 */
	void createLayerPath(int layer) {
		Path layerPath = this.path(layer);
		if (Files.notExists(layerPath)) {
			try {
				Files.createDirectories(layerPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Creates a new FileChannel for given layer, run and mode
	 * @param layer associated layer
	 * @param run associated run
	 * @param mode associated {@link IOMode}
	 * @return new FileChannel instance
	 * @throws IOException If an I/O error occurs
	 */
	FileChannel getRunFileChannel(int layer, int run, IOMode mode) throws IOException {
		return FileChannel.open(path(layer, run), mode.options());
	}

	/**
	 * Returns amount of runs for given layer
	 * @param layer associated layer
	 * @return amount of runs
	 */
	int getRunCountForLayer(int layer) {
		this.lock.lock();
		try {
			return this.metaFile.getRunCountForLayer(layer);
		} finally {
			this.lock.unlock();
		}
	}

	/**
	 * Returns next run for given layer
	 * @param layer associated layer
	 * @return next run id
	 */
	int nextRunCountForLayer(int layer) {
		this.lock.lock();
		try {
			return this.metaFile.nextRunCountForLayer(layer);
		} finally {
			this.lock.unlock();
		}
	}

	/**
	 * Adds new database and associated id to meta file
	 * @param database new database/table
	 * @param id id associated with database/table
	 */
	public void addDatabase(String database, int id) {
		this.lock.lock();
		try {
			this.metaFile.addDatabase(database, id);
		} finally {
			this.lock.unlock();
		}
	}

	/**
	 * Returns an {@link Iterable} for all database id pairs
	 * @return all database id pairs
	 */
	public Iterable<Entry<Integer, String>> getDatabaseIterable() {
		return this.metaFile.getDatabaseIterable();
	}

	/**
	 * Returns this basePaths {@link DeleteLayer}
	 * @return this {@link DeleteLayer}
	 */
	public DeleteLayer deleteLayer() {
		this.lock.lock();
		try {
			return new DeleteLayer(this);
		} finally {
			this.lock.unlock();
		}
	}

	/**
	 * Returns this basePaths last complete {@link PathLayer}
	 * @return last complete {@link PathLayer}
	 */
	public PathLayer lastLayer() {
		this.lock.lock();
		try {
			return new PathLayer(this, this.metaFile.getLayerCount() - 1);
		} finally {
			this.lock.unlock();
		}
	}

	/**
	 * Returns this basePaths next (incomplete) {@link PathLayer}
	 * @return next {@link PathLayer}
	 */
	public PathLayer nextLayer() {
		this.lock.lock();
		try {
			return new PathLayer(this, this.metaFile.getLayerCount());
		} finally {
			this.lock.unlock();
		}
	}

	public Path getBaseDir() {
		return baseDir.getFileName();
	}

	/**
	 * Writes the meta file to disk
	 */
	public void saveMeta() {
		this.lock.lock();
		try {
			this.metaFile.save();
		} finally {
			this.lock.unlock();
		}
	}

	@Override
	public String toString() {
		return "[baseDir=" + baseDir + ", metaFile=" + metaFile + "]";
	}
}
