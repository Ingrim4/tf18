package uni.leipzig.textmining.io.path;

import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Represents the delete layer in a basePath
 */
public class DeleteLayer {

	private static final int LAYER = -1;

	private final PathManager pathManager;

	DeleteLayer(PathManager pathManager) {
		this.pathManager = pathManager;
		this.pathManager.createLayerPath(LAYER);
	}

	/**
	 * Creates a read FileChannel for a given databaseId
	 * @param databaseId FileChannel's databaseId
	 * @return new read FileChannel
	 * @throws IOException If an I/O error occurs
	 */
	public FileChannel inputFileChannel(int databaseId) throws IOException {
		return this.pathManager.getRunFileChannel(LAYER, databaseId, IOMode.READ);
	}

	/**
	 * Creates a write FileChannel for given databaseId
	 * @param databaseId FileChannel's databaseId
	 * @return new write FileChannel
	 * @throws IOException If an I/O error occurs
	 */
	public FileChannel outputFileChannel(int databaseId) throws IOException {
		return this.pathManager.getRunFileChannel(LAYER, databaseId, IOMode.WRITE_DELETE);
	}
}
