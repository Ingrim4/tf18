package uni.leipzig.textmining.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import uni.leipzig.textmining.io.path.PathLayer;
import uni.leipzig.textmining.sentence.SentenceRow;
import uni.leipzig.textmining.sentence.SentenceRowSerializer;

/**
 * Sorted buffer of {@link SentenceRow}s that automatically flushes itself to disk whenever a certain threshold is reached
 */
public class SortedOutputBuffer {

	private final List<SentenceRow> elements;
	private final PathLayer pathLayer;

	private final ByteBuffer buffer;
	private final int size;

	/**
	 * Creates a new sorted buffer
	 * @param pathLayer the current path layer this buffer should write to
	 * @param size the threshold when the internal list should get flushed to disk
	 */
	public SortedOutputBuffer(PathLayer pathLayer, int size) {
		this.elements = new ArrayList<>();
		this.pathLayer = pathLayer;

		this.buffer = ByteBuffer.allocateDirect((int) (SentenceRowSerializer.ELEMENT_SIZE * Math.floor(size * 0.05)));
		this.size = size;
	}

	/**
	 * Sorts the internal list and then writes it to disk.
	 * Resets internal list and buffer after successful execution
	 */
	public void transferToFile() {
		try (FileChannel fileChannel = this.pathLayer.nextRunFileChannel()) {

			// dual pivot quick sort list
			Collections.sort(this.elements);

			// write till list is empty
			Iterator<SentenceRow> iterator = this.elements.iterator();
			while (iterator.hasNext()) {
				// write till buffer is full or no more data is available
				while (this.buffer.remaining() != 0 && iterator.hasNext()) {
					SentenceRowSerializer.INSTANCE.write(this.buffer, iterator.next());
				}

				// reset buffer position and limit to the amount of data contained in the buffer
				this.buffer.limit(this.buffer.capacity() - this.buffer.remaining());
				this.buffer.rewind();

				// write to file
				fileChannel.write(this.buffer);

				// reset buffer for next write
				this.buffer.clear();
			}

			// clear list since all data got written to file
			this.elements.clear();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Offer a new element to internal list. If the internal list reached its threshold it will call {@link SortedOutputBuffer#transferToFile()}
	 * @param element element to add to internal list
	 */
	public void offer(SentenceRow element) {
		this.elements.add(element);
		if (this.elements.size() >= this.size) {
			this.transferToFile();
		}
	}
}
