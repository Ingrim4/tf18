package uni.leipzig.textmining.io;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;

/**
 * Reads elements to internal (binary) buffer and auto reads more data when the buffer is empty.
 *
 * @param <E> the type of elements
 */
public class FileInputBuffer<E> implements Closeable {

	private final FileChannel fileChannel;
	private final Serializer<E> serializer;

	private final ByteBuffer buffer;
	private final int sizePerElement;

	private boolean hasNext = false;

	public FileInputBuffer(FileChannel fileChannel, Serializer<E> serializer, int size) throws IOException {
		this.fileChannel = fileChannel;
		this.serializer = serializer;

		this.sizePerElement = serializer.elementSize();
		this.buffer = ByteBuffer.allocateDirect(size * this.sizePerElement);
		this.readNextChunk();

	}

	/**
	 * Reads next chunk of data from disk to buffer if there is some. Updates
	 * {@code hasNext} to {@code true} if there is more data
	 */
	private void readNextChunk() {
		try {
			// reset buffer
			this.buffer.clear();

			// read next chunk from disk
			int limit = this.fileChannel.read(this.buffer);
			this.hasNext = limit > 0;
			if (!this.hasNext) {
				return;
			}

			// set buffer limit to amount of read bytes
			this.buffer.limit(limit);
			this.buffer.rewind();
		} catch (IOException e) {
			e.printStackTrace();
			this.hasNext = false;
		}
	}

	/**
	 * Reads the next element from buffer if there is one.
	 * 
	 * @throws NoSuchElementException if the buffer has no more elements
	 */
	public E next() {
		if (!this.hasNext) {
			throw new NoSuchElementException();
		}

		int positon = this.buffer.position();
		E element = this.serializer.read(buffer);
		this.buffer.position(positon + sizePerElement);

		if (this.buffer.remaining() == 0) {
			this.readNextChunk();
		}

		return element;
	}

	/**
	 * Returns {@code true} if the buffer has more elements.
	 * 
	 * @return {@code true} if the buffer has more elements
	 */
	public boolean hasNext() {
		return hasNext;
	}

	@Override
	public void close() throws IOException {
		this.fileChannel.close();
	}
}
