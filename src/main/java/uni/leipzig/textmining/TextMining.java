package uni.leipzig.textmining;

import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mariadb.jdbc.MariaDbPoolDataSource;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;

import uni.leipzig.textmining.command.CommandDelete;
import uni.leipzig.textmining.command.CommandManager;
import uni.leipzig.textmining.command.CommandMerge;
import uni.leipzig.textmining.command.CommandSelect;
import uni.leipzig.textmining.command.CommandSource;
import uni.leipzig.textmining.command.RegisterCommand;
import uni.leipzig.textmining.config.Config;
import uni.leipzig.textmining.filter.SentenceFilter;
import uni.leipzig.textmining.sentence.SentenceNormalizer;
import uni.leipzig.textmining.util.SimpleThreadFactory;

public class TextMining {

	private static final Logger LOGGER = LogManager.getLogger();

	public static void main(String[] args) {
		new TextMining();
	}

	private final ExecutorService taskService = Executors
			.newCachedThreadPool(new SimpleThreadFactory("task-thread", true));

	private final CommandManager commandManager;

	private final int threads;
	private final Config config;
	private final MariaDbPoolDataSource connectionPool;
	private final SentenceFilter sentenceFilter;
	private final SentenceNormalizer sentenceNormalizer;

	public TextMining() {
		this.threads = Runtime.getRuntime().availableProcessors();
		LOGGER.info("Using {} threads", this.threads);

		this.config = Config.getConfig();
		final Config.Database database = this.config.database;
		final Config.Filter filter = this.config.filter;
		final Config.Select select = this.config.select;

		LOGGER.info("Initializing command system...");
		this.commandManager = new CommandManager();
		this.commandManager.registerCommand(this);
		this.commandManager.registerCommand(new CommandSelect(this));
		this.commandManager.registerCommand(new CommandMerge(this));
		this.commandManager.registerCommand(new CommandDelete(this));

		LOGGER.info("Initializing database pool...");
		this.connectionPool = new MariaDbPoolDataSource(String.format("jdbc:mariadb://%s/?user=%s&password=%s",
				database.hostDescription, database.username, database.password));
		try {
			this.connectionPool.setMaxPoolSize(this.threads);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		LOGGER.info("Initializing sentence filter...");
		this.sentenceFilter = new SentenceFilter(filter);

		LOGGER.info("Initializing sentence normalizer...");
		this.sentenceNormalizer = new SentenceNormalizer(select.ignoreCharset);

		LOGGER.info("Ready!");
	}

	public ExecutorService getTaskService() {
		return taskService;
	}

	public int getThreads() {
		return threads;
	}

	public Config getConfig() {
		return config;
	}

	public MariaDbPoolDataSource getConnectionPool() {
		return connectionPool;
	}

	public SentenceFilter getSentenceFilter() {
		return sentenceFilter;
	}

	public SentenceNormalizer getSentenceNormalizer() {
		return sentenceNormalizer;
	}

	@RegisterCommand
	public void registerExitCommand(CommandDispatcher<CommandSource> dispatcher) {
		dispatcher.register(CommandManager.literal("exit").executes(context -> {
			this.taskService.shutdownNow();
			this.connectionPool.close();
			this.commandManager.close();
			return Command.SINGLE_SUCCESS;
		}));
	}
}
