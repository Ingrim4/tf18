package uni.leipzig.textmining.multimerge;

import java.util.List;
import java.util.PriorityQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uni.leipzig.textmining.io.DeleteOutputBuffer;
import uni.leipzig.textmining.io.FileInputBuffer;
import uni.leipzig.textmining.io.FileOutputBuffer;
import uni.leipzig.textmining.sentence.SentenceRow;
import uni.leipzig.textmining.task.MergeSubTask;
import uni.leipzig.textmining.util.statistics.MergeStatisticsReporter;

public class MultiwayMerge implements Runnable {

	private static final Logger LOGGER = LogManager.getLogger();

	private final MergeStatisticsReporter statisticsReporter;

	private final List<FileInputBuffer<SentenceRow>> inputBuffer;
	private final FileOutputBuffer<SentenceRow> outputBuffer;
	private final DeleteOutputBuffer deleteBuffer;

	private final PriorityQueue<Node> minHeap;
	private Node lastNode;

	public MultiwayMerge(MergeStatisticsReporter statisticsReporter, MergeSubTask task) {
		this.statisticsReporter = statisticsReporter;
		this.inputBuffer = task.inputBuffer();
		this.outputBuffer = task.outputBuffer();
		this.deleteBuffer = task.deleteBuffer();
		this.minHeap = new PriorityQueue<>(this.inputBuffer.size());
	}

	@Override
	public void run() {
		// Initialize min heap
		for (FileInputBuffer<SentenceRow> inputBuffer : this.inputBuffer) {
			if (inputBuffer.hasNext()) {
				this.minHeap.offer(new Node(inputBuffer));
			} else {
				LOGGER.error("input buffer is empty ({})", inputBuffer);
			}
		}

		// Merge until no data is left
		while (!this.minHeap.isEmpty()) {
			this.statisticsReporter.incrementMergeCount();

			// get smallest element
			Node minNode = this.minHeap.poll();
			SentenceRow element = minNode.get();

			// check if lastNode's hash is the same as our minNode's hash
			if (this.lastNode != null && this.lastNode.compareTo(minNode) == 0) {
				SentenceRow lastElement = this.lastNode.get();
				// check if the nodes are the same (a.databaseId == b.databaseId && a.sentenceId == b.sentenceId)
				if (lastElement.equals(element)) {
					LOGGER.warn("duplicate element ({} == {})", lastElement, element);
				} else {
					this.statisticsReporter.incrementDuplicateCount();
					this.deleteBuffer.offer(element);
				}
			} else {
				// offer the smallest element to our output buffer
				this.outputBuffer.offer(element);

				// update last node
				this.lastNode = minNode;
			}

			// if new data is available add it to min heap
			if (minNode.hasNext()) {
				this.minHeap.offer(minNode.next());
			// if no new data is available and there is only one element in the min heap left we can just dump the entire rest of the remaining buffer
			} else if (this.minHeap.size() == 1) {
				// offer last element in min heap to output buffer
				Node lastNode = this.minHeap.poll();
				this.statisticsReporter.incrementMergeCount();
				this.outputBuffer.offer(lastNode.get());

				// offer every remaining element in last buffer to outputbuffer
				FileInputBuffer<SentenceRow> inputBuffer = lastNode.parentBuffer();
				while (inputBuffer.hasNext()) {
					this.statisticsReporter.incrementMergeCount();
					this.outputBuffer.offer(inputBuffer.next());
				}
			}
		}
	}

	private static class Node implements Comparable<Node> {

		private final SentenceRow element;
		private final FileInputBuffer<SentenceRow> parentBuffer;

		public Node(FileInputBuffer<SentenceRow> parentBuffer) {
			this.element = parentBuffer.next();
			this.parentBuffer = parentBuffer;
		}

		public SentenceRow get() {
			return element;
		}

		public FileInputBuffer<SentenceRow> parentBuffer() {
			return this.parentBuffer;
		}

		public boolean hasNext() {
			return this.parentBuffer.hasNext();
		}

		public Node next() {
			return new Node(this.parentBuffer);
		}

		@Override
		public int compareTo(Node o) {
			return this.element.compareTo(o.element);
		}
	}
}
