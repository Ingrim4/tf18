package uni.leipzig.textmining.task;

import java.io.IOException;
import java.nio.channels.FileChannel;

import uni.leipzig.textmining.io.FileInputBuffer;
import uni.leipzig.textmining.io.path.DeleteLayer;
import uni.leipzig.textmining.sentence.SentenceIdSerializer;
import uni.leipzig.textmining.util.statistics.DeleteStatisticsReporter;

public class DeleteSubTask implements AutoCloseable {

	private final DeleteStatisticsReporter statistics;
	private final String sql;

	private final DeleteLayer deleteLayer;
	private final int databaseId;

	private FileInputBuffer<Integer> inputBuffer;

	public DeleteSubTask(DeleteStatisticsReporter statistics, String sql, DeleteLayer deleteLayer, int databaseId) {
		this.statistics = statistics;
		this.sql = sql;
		this.deleteLayer = deleteLayer;
		this.databaseId = databaseId;
	}

	public DeleteStatisticsReporter statistics() {
		return statistics;
	}

	public String sql() {
		return sql;
	}

	public FileInputBuffer<Integer> inputBuffer() {
		return inputBuffer;
	}

	void createBuffer(int size) {
		try {
			FileChannel fileChannel = this.deleteLayer.inputFileChannel(this.databaseId);
			this.inputBuffer = new FileInputBuffer<>(fileChannel, SentenceIdSerializer.INSTANCE, size);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		try {
			if (this.inputBuffer != null) {
				this.inputBuffer.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
