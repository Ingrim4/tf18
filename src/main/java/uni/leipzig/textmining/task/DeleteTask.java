package uni.leipzig.textmining.task;

import java.util.LinkedList;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uni.leipzig.textmining.TextMining;
import uni.leipzig.textmining.database.BatchDeleter;
import uni.leipzig.textmining.io.path.DeleteLayer;
import uni.leipzig.textmining.io.path.PathManager;
import uni.leipzig.textmining.util.statistics.DeleteStatisticsReporter;
import uni.leipzig.textmining.util.task.TaskPoolBuilder;
import uni.leipzig.textmining.util.task.TaskPoolFinalizer;
import uni.leipzig.textmining.util.task.TaskProducer;

public class DeleteTask {

	private static final Logger LOGGER = LogManager.getLogger();

	private final TextMining textMining;
	private final int threads;
	private final PathManager pathManager;

	private final DeleteStatisticsReporter statisticsReporter = new DeleteStatisticsReporter();
	private final LinkedList<DeleteSubTask> taskQueue = new LinkedList<>();

	public DeleteTask(TextMining textMining, PathManager pathManager) {
		this.textMining = textMining;
		this.threads = textMining.getThreads();
		this.pathManager = pathManager;
	}

	public void createSubTasks() {
		DeleteLayer deleteLayer = this.pathManager.deleteLayer();

		for (Entry<Integer, String> entry : this.pathManager.getDatabaseIterable()) {
			int databaseId = entry.getKey();
			String database = entry.getValue();

			String sql = String.format("DELETE FROM %s WHERE s_id = ?", database);
			this.taskQueue.offer(new DeleteSubTask(statisticsReporter, sql, deleteLayer, databaseId));
		}
	}

	public void start() {
		if (this.taskQueue.isEmpty()) {
			LOGGER.warn("taskQueue is empty ({} == 0)", this.taskQueue.isEmpty());
			return;
		}

		System.out.println();
		this.statisticsReporter.start();

		new TaskPoolBuilder<DeleteSubTask>()
			.poolSize(this.threads)
			.taskFinalizer(DeleteSubTask::close)
			.poolFinalizer(this.finalizer())
			.build(this.taskQueue, this.producer());
	}

	private TaskProducer<DeleteSubTask> producer() {
		int elementsPerBuffer = this.textMining.getConfig().database.batchSize;
		return (oldTask, newTask) -> {
			if (oldTask != null) {
				oldTask.close();
			}

			newTask.createBuffer(elementsPerBuffer);
			return new BatchDeleter(this.textMining, newTask);
		};
	}

	private TaskPoolFinalizer finalizer() {
		return () -> {
			this.statisticsReporter.close();
			LOGGER.info("Delete done!");
		};
	}
}
