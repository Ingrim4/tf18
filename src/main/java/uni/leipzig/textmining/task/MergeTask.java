package uni.leipzig.textmining.task;

import java.util.LinkedList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uni.leipzig.textmining.TextMining;
import uni.leipzig.textmining.config.Config;
import uni.leipzig.textmining.io.DeleteOutputBuffer;
import uni.leipzig.textmining.io.path.DeleteLayer;
import uni.leipzig.textmining.io.path.PathLayer;
import uni.leipzig.textmining.io.path.PathManager;
import uni.leipzig.textmining.multimerge.MultiwayMerge;
import uni.leipzig.textmining.sentence.SentenceRowSerializer;
import uni.leipzig.textmining.util.statistics.MergeStatisticsReporter;
import uni.leipzig.textmining.util.task.TaskPoolBuilder;
import uni.leipzig.textmining.util.task.TaskPoolFinalizer;
import uni.leipzig.textmining.util.task.TaskProducer;

public class MergeTask {

	private static final Logger LOGGER = LogManager.getLogger();

	private final TextMining textMining;
	private final int threads;
	private final PathManager pathManager;

	private final MergeStatisticsReporter statisticsReporter = new MergeStatisticsReporter();
	private final LinkedList<MergeSubTask> taskQueue = new LinkedList<>();

	public MergeTask(TextMining textMining, PathManager pathManager) {
		this.textMining = textMining;
		this.threads = textMining.getThreads();
		this.pathManager = pathManager;
	}

	public void createSubTasks() {
		PathLayer layer = pathManager.lastLayer();
		int runs = layer.getRunCount();

		PathLayer nextLayer = pathManager.nextLayer();
		nextLayer.createLayerPath();

		final int runsPerMerge = this.textMining.getConfig().merge.runsPerMerge;
		for (int run = 0; run <= runs; run++) {
			if (run % runsPerMerge == 0) {
				this.taskQueue.offer(new MergeSubTask(layer, nextLayer));
			}
			this.taskQueue.peekLast().run(run);
		}
	}

	public void start() {
		if (this.taskQueue.isEmpty()) {
			LOGGER.warn("taskQueue is empty ({} == 0)", this.taskQueue.isEmpty());
			return;
		}

		final Config.Merge mergeSort = this.textMining.getConfig().merge;
		final int heapSize = mergeSort.heapSizeMerge;
		final int runsPerMerge = mergeSort.runsPerMerge;

		final int threads = Math.min(this.threads, this.taskQueue.size());
		final int bytesPerThread = heapSize / threads;
		final int bytesPerBuffer = bytesPerThread / (runsPerMerge + 1/* output */);
		final int elementsPerBuffer = bytesPerBuffer / SentenceRowSerializer.ELEMENT_SIZE;

		DeleteLayer deleteLayer = pathManager.deleteLayer();
		DeleteOutputBuffer deleteBuffer = new DeleteOutputBuffer(deleteLayer, 1000); // TODO dynamic

		LOGGER.info("Merge will create {} run files", this.taskQueue.size());

		System.out.println();
		this.statisticsReporter.start();

		new TaskPoolBuilder<MergeSubTask>()
			.poolSize(this.threads)
			.taskFinalizer(MergeSubTask::close)
			.poolFinalizer(this.finalizer(deleteBuffer))
			.build(this.taskQueue, this.producer(elementsPerBuffer, deleteBuffer));
	}

	private TaskProducer<MergeSubTask> producer(int elementsPerBuffer, DeleteOutputBuffer deleteBuffer) {
		return (oldTask, newTask) -> {
			if (oldTask != null) {
				oldTask.close();
			}

			newTask.createBuffer(elementsPerBuffer);
			newTask.deleteBuffer(deleteBuffer);
			return new MultiwayMerge(this.statisticsReporter, newTask);
		};
	}

	private TaskPoolFinalizer finalizer(DeleteOutputBuffer deleteBuffer) {
		return () -> {
			this.statisticsReporter.close();
			this.pathManager.saveMeta();
			deleteBuffer.close();
			LOGGER.info("Merge done!");
		};
	}
}
