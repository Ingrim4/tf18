package uni.leipzig.textmining.task;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mariadb.jdbc.MariaDbPoolDataSource;

import uni.leipzig.textmining.TextMining;
import uni.leipzig.textmining.database.BatchSelector;
import uni.leipzig.textmining.io.DeleteOutputBuffer;
import uni.leipzig.textmining.io.SortedOutputBuffer;
import uni.leipzig.textmining.io.path.DeleteLayer;
import uni.leipzig.textmining.io.path.PathLayer;
import uni.leipzig.textmining.io.path.PathManager;
import uni.leipzig.textmining.util.statistics.SelectStatisticsReporter;
import uni.leipzig.textmining.util.statistics.TableStatistics;
import uni.leipzig.textmining.util.task.TaskFinalizer;
import uni.leipzig.textmining.util.task.TaskPoolBuilder;
import uni.leipzig.textmining.util.task.TaskPoolFinalizer;
import uni.leipzig.textmining.util.task.TaskProducer;

public class SelectorTask {

	private static final Logger LOGGER = LogManager.getLogger();

	private final TextMining textMining;
	private final MariaDbPoolDataSource connectionPool;

	private final PathManager pathManager;
	private final PathLayer pathLayer;

	private final int threads;

	private final SelectStatisticsReporter statisticsReporter;
	private final Queue<SelectorSubTask> taskQueue = new LinkedList<>();

	public SelectorTask(TextMining textMining) {
		this.textMining = textMining;
		this.threads = textMining.getThreads();
		this.connectionPool = textMining.getConnectionPool();
		this.statisticsReporter = new SelectStatisticsReporter(this.threads);

		this.pathManager = PathManager.createNow();
		this.pathLayer = this.pathManager.nextLayer();
		this.pathLayer.createLayerPath();
	}

	public void createSubTasks(List<String> databaseList) {
		// iterate over all databases/tables
		for (int databaseId = 0; databaseId < databaseList.size(); databaseId++) {
			final String database = databaseList.get(databaseId);

			// get the rowCount of the current database
			final int rowCount = this.getRowCount(database);
			if (rowCount == 0) {
				LOGGER.warn("table {} is empty", database);
				continue;
			}

			// create statistics object and add it to statisticsReporter
			final TableStatistics statistics = new TableStatistics(database, rowCount, this.threads);
			this.statisticsReporter.add(statistics);

			final int batchSize = rowCount / this.threads;
			final int remainder = rowCount % batchSize;

			// create 'thread' times sub tasks
			for (int i = 0; i < this.threads; i++) {
				int limit = i != (this.threads - 1) ? batchSize : batchSize + remainder;
				String sql = String.format("SELECT * FROM %s LIMIT %d OFFSET %d", database, limit, i * batchSize);

				this.taskQueue.offer(new SelectorSubTask(databaseId, statistics, sql));
			}

			// add database-id pair to pathManager (meta file)
			this.pathManager.addDatabase(database, databaseId);
		}
	}

	/**
	 * Returns row count of a table
	 * @param table table to get row count of
	 * @return row count
	 */
	private int getRowCount(String table) {
		try (Connection connection = this.connectionPool.getConnection()) {
			try (Statement statement = connection.createStatement()) {
				try (ResultSet resultSet = statement.executeQuery(String.format("SELECT COUNT(*) FROM %s", table))) {
					if (resultSet.next()) {
						return resultSet.getInt("COUNT(*)");
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void start() {
		if (this.taskQueue.isEmpty()) {
			LOGGER.warn("taskQueue is empty ({} == 0)", this.taskQueue.isEmpty());
			return;
		}

		// create DeleteOutputBuffer
		DeleteLayer deleteLayer = pathManager.deleteLayer();
		DeleteOutputBuffer deleteBuffer = new DeleteOutputBuffer(deleteLayer, 1000); // TODO dynamic

		// print base
		LOGGER.info("Select basePath is: \"{}\"", this.pathManager.getBaseDir());
		System.out.println();

		// start statisticsReporter scheduled task
		this.statisticsReporter.start();

		// build task pool
		new TaskPoolBuilder<SelectorSubTask>()
			.poolSize(this.threads)
			.taskFinalizer(this.finalizerTask())
			.poolFinalizer(this.finalizer())
			.build(this.taskQueue, this.producer(deleteBuffer));
	}

	private TaskProducer<SelectorSubTask> producer(DeleteOutputBuffer deleteBuffer) {
		final int bufferSize = this.textMining.getConfig().select.elementsPerRuns;
		return (oldTask, newTask) -> {
			if (oldTask == null) {
				// create new sorted output buffer for first task per thread
				newTask.sortedBuffer(new SortedOutputBuffer(this.pathLayer, bufferSize));
			} else {
				// pass on sorted buffer from previous task to next task
				oldTask.statistics().finish();
				newTask.sortedBuffer(oldTask.sortedBuffer());
			}

			// set delete buffer for next task
			newTask.deleteBuffer(deleteBuffer);

			// return new batch selector as runnable
			return new BatchSelector(this.textMining, newTask);
		};
	}

	private TaskFinalizer<SelectorSubTask> finalizerTask() {
		return (task) -> {
			// flush the remaining data in sorted buffer to disk
			task.statistics().finish();
			task.sortedBuffer().transferToFile();
		};
	}

	private TaskPoolFinalizer finalizer() {
		return () -> {
			// close all open resources
			this.statisticsReporter.close();
			this.pathManager.saveMeta();
			LOGGER.info("Select done!");
		};
	}
}
