package uni.leipzig.textmining.task;

import uni.leipzig.textmining.io.DeleteOutputBuffer;
import uni.leipzig.textmining.io.SortedOutputBuffer;
import uni.leipzig.textmining.util.statistics.TableStatistics;

public class SelectorSubTask {

	private final int databaseId;
	private final TableStatistics statistics;
	private final String sql;

	private SortedOutputBuffer sortedBuffer;
	private DeleteOutputBuffer deleteBuffer;

	public SelectorSubTask(int databaseId, TableStatistics statistics, String sql) {
		this.databaseId = databaseId;
		this.statistics = statistics;
		this.sql = sql;
	}

	public int databaseId() {
		return databaseId;
	}

	public TableStatistics statistics() {
		return statistics;
	}

	public String sql() {
		return sql;
	}

	public SortedOutputBuffer sortedBuffer() {
		return sortedBuffer;
	}

	public void sortedBuffer(SortedOutputBuffer sortedBuffer) {
		this.sortedBuffer = sortedBuffer;
	}

	void deleteBuffer(DeleteOutputBuffer deleteBuffer) {
		this.deleteBuffer = deleteBuffer;
	}

	public DeleteOutputBuffer deleteBuffer() {
		return deleteBuffer;
	}
}
