package uni.leipzig.textmining.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import uni.leipzig.textmining.io.DeleteOutputBuffer;
import uni.leipzig.textmining.io.FileInputBuffer;
import uni.leipzig.textmining.io.FileOutputBuffer;
import uni.leipzig.textmining.io.path.PathLayer;
import uni.leipzig.textmining.sentence.SentenceRow;
import uni.leipzig.textmining.sentence.SentenceRowSerializer;

public class MergeSubTask implements AutoCloseable {

	private final PathLayer inputLayer;
	private final PathLayer outputLayer;
	private final List<Integer> runs;

	private List<FileInputBuffer<SentenceRow>> inputBuffers = new ArrayList<>();
	private FileOutputBuffer<SentenceRow> outputBuffer;
	private DeleteOutputBuffer deleteBuffer;

	public MergeSubTask(PathLayer inputLayer, PathLayer outputLayer) {
		this.inputLayer = inputLayer;
		this.outputLayer = outputLayer;
		this.runs = new ArrayList<>();
	}

	void run(int run) {
		this.runs.add(run);
	}

	public List<FileInputBuffer<SentenceRow>> inputBuffer() {
		return inputBuffers;
	}

	public FileOutputBuffer<SentenceRow> outputBuffer() {
		return outputBuffer;
	}

	void createBuffer(int size) {
		try {
			for (int run : this.runs) {
				this.inputBuffers.add(new FileInputBuffer<>(this.inputLayer.getRunFileChannel(run), SentenceRowSerializer.INSTANCE, size));
			}

			this.outputBuffer = new FileOutputBuffer<>(this.outputLayer.nextRunFileChannel(), SentenceRowSerializer.INSTANCE, size);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void deleteBuffer(DeleteOutputBuffer deleteBuffer) {
		this.deleteBuffer = deleteBuffer;
	}

	public DeleteOutputBuffer deleteBuffer() {
		return deleteBuffer;
	}

	@Override
	public void close() {
		try {
			for (FileInputBuffer<SentenceRow> inputBuffer : this.inputBuffers) {
				inputBuffer.close();
			}
			if (this.outputBuffer != null) {
				this.outputBuffer.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
