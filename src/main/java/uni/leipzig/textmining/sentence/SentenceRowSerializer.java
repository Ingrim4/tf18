package uni.leipzig.textmining.sentence;

import java.nio.ByteBuffer;

import uni.leipzig.textmining.io.Serializer;

/**
 * Serializer for SentenceRows (databaseId int16, sentenceId int32, hash byte[])
 */
public class SentenceRowSerializer implements Serializer<SentenceRow> {

	public static final int ELEMENT_SIZE = 6 + SentenceRow.HASH_SIZE_IN_BYTES;
	public static final SentenceRowSerializer INSTANCE = new SentenceRowSerializer();

	@Override
	public int elementSize() {
		return ELEMENT_SIZE;
	}

	@Override
	public SentenceRow read(ByteBuffer buffer) {
		short databaseId = buffer.getShort();
		int sentenceId = buffer.getInt();

		byte[] hash = new byte[SentenceRow.HASH_SIZE_IN_BYTES];
		buffer.get(hash);

		return new SentenceRow(databaseId, sentenceId, hash);
	}

	@Override
	public void write(ByteBuffer buffer, SentenceRow element) {
		buffer.putShort((short) element.databaseId);
		buffer.putInt(element.sentenceId);
		buffer.put(element.hash);
	}
}
