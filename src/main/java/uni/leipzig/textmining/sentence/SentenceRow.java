package uni.leipzig.textmining.sentence;

import java.nio.charset.StandardCharsets;
import java.util.Comparator;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.common.primitives.UnsignedBytes;

public class SentenceRow implements Comparable<SentenceRow> {

	private static final Comparator<byte[]> BYTES_COMPERATOR = UnsignedBytes.lexicographicalComparator();

	private static final HashFunction HASH_FUNCTION = Hashing.murmur3_128();
	static final int HASH_SIZE_IN_BYTES = HASH_FUNCTION.bits() / 8;

	public static byte[] hash(String sentence) {
		return HASH_FUNCTION.hashBytes(sentence.getBytes(StandardCharsets.UTF_8)).asBytes();
	}

	public int databaseId;
	public int sentenceId;
	public byte[] hash;

	public SentenceRow(int databaseId, int sentenceId, byte[] hash) {
		this.databaseId = databaseId;
		this.sentenceId = sentenceId;
		this.hash = hash;
	}

	@Override
	public int compareTo(SentenceRow other) {
		return BYTES_COMPERATOR.compare(this.hash, other.hash);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (!(obj instanceof SentenceRow)) {
			return false;
		} else {
			SentenceRow other = (SentenceRow) obj;
			return this.databaseId == other.databaseId && this.sentenceId == other.sentenceId;
		}
	}

	@Override
	public String toString() {
		return "[databaseId=" + this.databaseId + ", sentenceId=" + this.sentenceId + "]";
	}
}
