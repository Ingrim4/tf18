package uni.leipzig.textmining.sentence;

import java.nio.ByteBuffer;

import uni.leipzig.textmining.io.Serializer;

/**
 * Serializer for sentence-ids (plain int32 linear list)
 */
public class SentenceIdSerializer implements Serializer<Integer> {

	public static final SentenceIdSerializer INSTANCE = new SentenceIdSerializer();

	@Override
	public int elementSize() {
		return 4;
	}

	@Override
	public Integer read(ByteBuffer buffer) {
		return buffer.getInt();
	}

	@Override
	public void write(ByteBuffer buffer, Integer element) {
		buffer.putInt(element);
	}
}
