package uni.leipzig.textmining.sentence;

import java.util.BitSet;

/**
 * Normalizes sentences by replacing all numbers with '0' and removing ignored characters
 */
public class SentenceNormalizer {

	private final BitSet ignoreCharset = new BitSet();

	/**
	 * Creates a new SentenceNormalizer with given ignore charset
	 * @param ignoreCharset charset of characters to ignore
	 */
	public SentenceNormalizer(String ignoreCharset) {
		for (int i = 0; i < ignoreCharset.length(); i++) {
			this.ignoreCharset.set(ignoreCharset.charAt(i));
		}
	}

	/**
	 * Replaces all numbers with '0' and removed ignored characters
	 * @param sentence sentence to normalize
	 * @return normalized sentence
	 */
	public String normalize(String sentence) {
		int length = sentence.length();
		char[] buffer = new char[length];

		int bufferIndex = 0;
		for (int i = 0; i < length; i++) {
			char c = sentence.charAt(i);
			if (Character.isDigit(c)) {
				c = '0';
			} else if (ignoreCharset.get(c)) {
				continue;
			}
			buffer[bufferIndex++] = c;
		}

		return new String(buffer, 0, bufferIndex);
	}
}
