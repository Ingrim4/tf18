package uni.leipzig.textmining.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.mariadb.jdbc.MariaDbPoolDataSource;

import uni.leipzig.textmining.TextMining;
import uni.leipzig.textmining.io.FileInputBuffer;
import uni.leipzig.textmining.task.DeleteSubTask;
import uni.leipzig.textmining.util.statistics.DeleteStatisticsReporter;

public class BatchDeleter implements Runnable {

	private final MariaDbPoolDataSource connectionPool;
	private final int batchSize;

	private final DeleteStatisticsReporter statistics;
	private final FileInputBuffer<Integer> inputBuffer;
	private final String sql;

	public BatchDeleter(TextMining textMining, DeleteSubTask deleteTask) {
		this.connectionPool = textMining.getConnectionPool();
		this.batchSize = textMining.getConfig().database.batchSize;

		this.statistics = deleteTask.statistics();
		this.inputBuffer = deleteTask.inputBuffer();
		this.sql = deleteTask.sql();
	}

	@Override
	public void run() {
		try (Connection connection = this.connectionPool.getConnection()) {
			// delete data until there is non left
			while (this.inputBuffer.hasNext()) {
				try (PreparedStatement statement = connection.prepareStatement(this.sql)) {
					// add new batches until we reach the batchSize and execute the batch
					for (int i = 0; i < this.batchSize && this.inputBuffer.hasNext(); i++) {
						statement.setInt(1, this.inputBuffer.next());
						statement.addBatch();
						this.statistics.incrementDeleteCount();
					}
					statement.executeBatch();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
