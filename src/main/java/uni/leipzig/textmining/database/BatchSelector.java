package uni.leipzig.textmining.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.mariadb.jdbc.MariaDbPoolDataSource;

import uni.leipzig.textmining.TextMining;
import uni.leipzig.textmining.filter.SentenceFilter;
import uni.leipzig.textmining.io.DeleteOutputBuffer;
import uni.leipzig.textmining.io.SortedOutputBuffer;
import uni.leipzig.textmining.sentence.SentenceNormalizer;
import uni.leipzig.textmining.sentence.SentenceRow;
import uni.leipzig.textmining.task.SelectorSubTask;
import uni.leipzig.textmining.util.statistics.TableStatistics;

public class BatchSelector implements Runnable {

	private final MariaDbPoolDataSource connectionPool;
	private final int batchSize;

	private final SentenceFilter sentenceFilter;
	private final SentenceNormalizer sentenceNormalizer;

	private final TableStatistics statistics;
	private final SortedOutputBuffer sortedBuffer;
	private final DeleteOutputBuffer deleteBuffer;

	private final int databaseId;
	private final String sql;

	public BatchSelector(TextMining textMining, SelectorSubTask selectTask) {
		this.connectionPool = textMining.getConnectionPool();
		this.batchSize = textMining.getConfig().database.batchSize;

		this.sentenceFilter = textMining.getSentenceFilter();
		this.sentenceNormalizer = textMining.getSentenceNormalizer();

		this.statistics = selectTask.statistics();
		this.sortedBuffer = selectTask.sortedBuffer();
		this.deleteBuffer = selectTask.deleteBuffer();

		this.databaseId = selectTask.databaseId();
		this.sql = selectTask.sql();
	}

	private void select(ResultSet resultSet) throws SQLException {
		int sentenceId = resultSet.getInt("s_id");
		String sentence = resultSet.getString("sentence");

		// filter sentence and write to delete buffer if fail
		if (!this.sentenceFilter.test(sentence, this.statistics)) {
			this.deleteBuffer.offer(this.databaseId, sentenceId);
			return;
		}

		// normalize sentence and hash it
		byte[] hash = SentenceRow.hash(this.sentenceNormalizer.normalize(sentence));

		// offer new SentenceRow to sortedBuffer
		this.statistics.incrementSortedCount();
		this.sortedBuffer.offer(new SentenceRow(databaseId, sentenceId, hash));
	}

	@Override
	public void run() {
		try (Connection connection = this.connectionPool.getConnection()) {
			try (Statement statement = connection.createStatement()) {
				// set the fetch size so the threads isn't blocking the entire time the statement is fetched
				statement.setFetchSize(this.batchSize);
				try (ResultSet resultSet = statement.executeQuery(this.sql)) {
					while (resultSet.next()) {
						this.statistics.incrementSelectCount();
						this.select(resultSet);
					}
				}	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
