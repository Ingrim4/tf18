package uni.leipzig.textmining.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import uni.leipzig.textmining.TextMining;
import uni.leipzig.textmining.io.path.PathManager;
import uni.leipzig.textmining.task.DeleteTask;

public class CommandDelete {

	private final TextMining textMining;

	public CommandDelete(TextMining textMining) {
		this.textMining = textMining;
	}

	@RegisterCommand
	public void registerDeleteCommand(CommandDispatcher<CommandSource> dispatcher) {
		dispatcher.register(CommandManager.literal("delete")
				.then(CommandManager.argument("base", StringArgumentType.word()).executes(this::delete)));
	}

	private int delete(CommandContext<CommandSource> context) throws CommandSyntaxException {
		String base = StringArgumentType.getString(context, "base");
		PathManager pathManager = PathManager.create(base);
		if (pathManager == null) {
			throw CommandManager.UNKNOWN_BASE.create(base);
		}

		DeleteTask deleteTask = new DeleteTask(this.textMining, pathManager);
		deleteTask.createSubTasks();
		this.textMining.getTaskService().execute(deleteTask::start);

		return Command.SINGLE_SUCCESS;
	}
}
