package uni.leipzig.textmining.command;

import java.util.Collection;
import java.util.List;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.ParseResults;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.context.ParsedCommandNode;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

public class CommandHelp {

	@RegisterCommand
	public static void register(CommandDispatcher<CommandSource> dispatcher) {
		dispatcher.register(CommandManager.literal("help").executes(context -> {
			Collection<String> usages = dispatcher.getSmartUsage(dispatcher.getRoot(), context.getSource()).values();
			for (String usage : usages) {
				context.getSource().sendMessage(usage);
			}
			return usages.size();
		}).then(CommandManager.argument("command", StringArgumentType.greedyString()).executes(context -> {
			ParseResults<CommandSource> command = dispatcher.parse(StringArgumentType.getString(context, "command"), context.getSource());
			List<ParsedCommandNode<CommandSource>> nodes = command.getContext().getNodes();

			if (nodes.isEmpty()) {
				throw CommandSyntaxException.BUILT_IN_EXCEPTIONS.dispatcherUnknownCommand().create();
			} else {
				Collection<String> usages = dispatcher.getSmartUsage(nodes.get(nodes.size() - 1).getNode(), context.getSource()).values();
				for (String usage : usages) {
					context.getSource().sendMessage(usage);
				}
				return usages.size();
			}
		})));
	}
}
