package uni.leipzig.textmining.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.LiteralMessage;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;

public class CommandManager {

	public static final DynamicCommandExceptionType UNKNOWN_BASE = new DynamicCommandExceptionType(
			run -> new LiteralMessage("base path doesn't exist: " + run));

	public static LiteralArgumentBuilder<CommandSource> literal(String name) {
		return LiteralArgumentBuilder.literal(name);
	}

	public static <T> RequiredArgumentBuilder<CommandSource, T> argument(String name, ArgumentType<T> type) {
		return RequiredArgumentBuilder.argument(name, type);
	}

	private final CommandDispatcher<CommandSource> dispatcher = new CommandDispatcher<>();
	private final ConsoleCommandSource console = new ConsoleCommandSource();

	private boolean running = true;

	public CommandManager() {
		this.registerCommand(CommandHelp.class);

		Thread commandThread = new Thread(() -> {
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
				String input = "";
				while (this.running && (input = reader.readLine()) != null) {
					this.issueCommand(this.console, input);
				}
			} catch (IOException e) {
				throw new RuntimeException("Exception handling console input", e);
			}
		}, "console-reader");

//		commandThread.setDaemon(true);
		commandThread.start();
	}

	public int issueCommand(CommandSource source, String command) {
		try {
			return this.dispatcher.execute(command, source);
		} catch (CommandSyntaxException e) {
			source.sendMessage(e.getMessage());
		} catch (Exception e) {
			source.sendMessage(e.getMessage() != null ? e.getMessage() : e.getClass().getName());
			e.printStackTrace();
		}
		return 0;
	}

	public void registerCommand(final Object target) {
		if (target.getClass() == Class.class) {
			this.registerClass((Class<?>) target);
		} else {
			this.registerObject(target);
		}
	}


	private void registerClass(final Class<?> target) {
		for (final Method method : target.getMethods()) {
			if (Modifier.isStatic(method.getModifiers()) && method.isAnnotationPresent(RegisterCommand.class)) {
				this.registerCommand0(target, method);
			}
		}
	}

	private void registerObject(final Object target) {
		for (final Method method : target.getClass().getMethods()) {
			if (!Modifier.isStatic(method.getModifiers()) && method.isAnnotationPresent(RegisterCommand.class)) {
				this.registerCommand0(target, method);
			}
		}
	}

	private void registerCommand0(Object target, Method method) {
		final Class<?>[] parameterTypes = method.getParameterTypes();
		if (parameterTypes.length != 1) {
			throw new IllegalArgumentException(
				"Method " + method + " has @RegisterCommand annotation. " +
				"It has " + parameterTypes.length + " arguments, " +
				"but event handler methods require a single argument only."
			);
		}

		final Class<?> parameterClass = parameterTypes[0];
		if (!CommandDispatcher.class.isAssignableFrom(parameterClass)) {
			throw new IllegalArgumentException(
				"Method " + method + " has @RegisterCommand annotation, " +
				"but takes an argument that is not an CommandDispatcher : " + parameterClass
			);
		}

		try {
			method.invoke(target, this.dispatcher);
		} catch (IllegalAccessException e) {
			throw new IllegalStateException("Cannot use reflection.", e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException("An internal error occured.", e.getCause());
		} catch (IllegalArgumentException e) {
			throw e; // This shouldn't happen
		}
	}

	public ConsoleCommandSource getConsole() {
		return console;
	}

	public void close() {
		this.running = false;
	}
}
