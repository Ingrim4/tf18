package uni.leipzig.textmining.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import uni.leipzig.textmining.TextMining;
import uni.leipzig.textmining.io.path.PathManager;
import uni.leipzig.textmining.task.MergeTask;

public class CommandMerge {

	private final TextMining textMining;

	public CommandMerge(TextMining textMining) {
		this.textMining = textMining;
	}

	@RegisterCommand
	public void registerMergeCommand(CommandDispatcher<CommandSource> dispatcher) {
		dispatcher.register(CommandManager.literal("merge")
				.then(CommandManager.argument("base", StringArgumentType.word()).executes(this::merge)));
	}

	private int merge(CommandContext<CommandSource> context) throws CommandSyntaxException {
		String base = StringArgumentType.getString(context, "base");
		PathManager pathManager = PathManager.create(base);
		if (pathManager == null) {
			throw CommandManager.UNKNOWN_BASE.create(base);
		}

		MergeTask mergeTask = new MergeTask(this.textMining, pathManager);
		mergeTask.createSubTasks();
		this.textMining.getTaskService().execute(mergeTask::start);

		return Command.SINGLE_SUCCESS;
	}
}
