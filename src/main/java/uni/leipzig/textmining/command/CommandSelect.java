package uni.leipzig.textmining.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;

import uni.leipzig.textmining.TextMining;
import uni.leipzig.textmining.task.SelectorTask;

public class CommandSelect {

	private final TextMining textMining;

	public CommandSelect(TextMining textMining) {
		this.textMining = textMining;
	}

	@RegisterCommand
	public void registerSelectCommand(CommandDispatcher<CommandSource> dispatcher) {
		dispatcher.register(CommandManager.literal("select").executes(context -> {

			SelectorTask selectorPool = new SelectorTask(this.textMining);
			selectorPool.createSubTasks(this.textMining.getConfig().tables);
			this.textMining.getTaskService().execute(selectorPool::start);

			return Command.SINGLE_SUCCESS;
		}));
	}
}
