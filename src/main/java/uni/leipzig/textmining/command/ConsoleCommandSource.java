package uni.leipzig.textmining.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleCommandSource implements CommandSource {

	private Logger LOGGER = LogManager.getLogger();

	@Override
	public String getName() {
		return "CONSOLE";
	}

	@Override
	public void sendMessage(String message) {
		LOGGER.info(message);
	}

}
