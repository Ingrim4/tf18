package uni.leipzig.textmining.command;

public interface CommandSource {

	String getName();

	void sendMessage(String message);
}
