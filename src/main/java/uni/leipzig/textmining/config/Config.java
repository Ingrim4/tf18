package uni.leipzig.textmining.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.logging.log4j.core.appender.FileManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Config {

	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

	private static final Path PATH = Paths.get("config.json");

	public Database database;
	public Filter filter;
	public Select select;
	public Merge merge;
	public List<String> tables;

	public class Database {
		public String hostDescription;
		public String username;
		public String password;
		public int batchSize; // batch size for select/delete
	}

	public class Filter {
		public int length; // min length a sentence has to have
		public int repeat; // number of characters at the beginning of sentence that aren't allowed to be the same
		public String start; // allowed characters at start of sentence
		public String characters; // allowed characters in sentence
	}

	public class Select {
		public String ignoreCharset; // characters to be removed in normalize step
		public int elementsPerRuns; // elements per sorted buffer
	}

	public class Merge {
		public int runsPerMerge; // runs (number of input buffer) per multi-merge
		public int heapSizeMerge; // heap size while merge step
	}

	public static Config getConfig() {
		try {
			if (Files.notExists(Config.PATH)) {
				Files.copy(FileManager.class.getResourceAsStream("/configuration/config.json"), Config.PATH);
			}

			try (BufferedReader configReader = Files.newBufferedReader(Config.PATH)) {
				return Config.GSON.fromJson(configReader, Config.class);
			}
		} catch (IOException e) {
			throw new RuntimeException("Unable to load config", e);
		}
	}
}
