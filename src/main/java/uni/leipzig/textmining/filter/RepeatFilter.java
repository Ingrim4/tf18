package uni.leipzig.textmining.filter;

import java.util.function.Predicate;

/**
 * Predicate for testing string beginning for repeating characters
 * 
 * <p>
 * A string will only pass when its first {@code repeat} characters aren't equal
 * </p>
 */
public class RepeatFilter implements Predicate<String> {

	private final int repeat;

	/**
	 * Constructs a new RepeatFilter
	 * 
	 * @param repeat number of characters that shouldn't equal
	 */
	public RepeatFilter(int repeat) {
		if (repeat < 2) {
			throw new IllegalArgumentException(
					"repeat(" + repeat + ") can't be smaller than 2");
		}
		this.repeat = repeat;
	}

	/**
	 * @return {@code true} if the first {@code repeat} characters aren't equal
	 *         otherwise {@code false}
	 */
	@Override
	public boolean test(String text) {
		if (text.length() < this.repeat) {
			return true;
		}

		char firstChar = text.charAt(0);
		for (int i = 1; i < this.repeat; i++) {
			if (firstChar != text.charAt(i)) {
				return true;
			}
		}

		return false;
	}

}
