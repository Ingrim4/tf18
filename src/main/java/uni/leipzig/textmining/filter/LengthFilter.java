package uni.leipzig.textmining.filter;

import java.util.function.Predicate;

/**
 * Predicate for testing string length
 * 
 * <p>
 * A string will only pass when its length is between minLength and maxLength
 * including the bounds.
 * </p>
 */
public class LengthFilter implements Predicate<String> {

	private final int minLength;
	private final int maxLength;

	/**
	 * Constructs a new LengthFilter
	 * 
	 * @param minLength lower bound
	 * @param maxLength upper bound
	 */
	public LengthFilter(int minLength, int maxLength) {
		if (minLength < 0 || maxLength < 0) {
			throw new IllegalArgumentException(
					"minLength(" + minLength + ") and maxLength(" + maxLength + ") can't be smaller than 0");
		}
		if (minLength > maxLength) {
			throw new IllegalArgumentException(
					"minLength(" + minLength + ") can't be bigger than maxLength(" + maxLength + ")");
		}

		this.minLength = minLength;
		this.maxLength = maxLength;
	}

	/**
	 * @return {@code true} if length is element of {@code [minLength, maxLength]}
	 *         otherwise {@code false}
	 */
	@Override
	public boolean test(String text) {
		final int length = text.length();
		return length >= this.minLength && length <= this.maxLength;
	}
}
