package uni.leipzig.textmining.filter;

import java.util.BitSet;
import java.util.function.Predicate;

/**
 * Predicate for testing string to match a set of characters
 * 
 * <p>
 * A string will only pass when all of its characters are in the given set
 * </p>
 */
public class CharacterFilter implements Predicate<String> {

	private final BitSet characters;

	/**
	 * Constructs a new CharacterFilter
	 * 
	 * @param characters allowed set of characters
	 */
	public CharacterFilter(BitSet characters) {
		this.characters = characters;
	}

	/**
	 * Returns a {@code CharacterFilter} with arbitrary long character set
	 * @param characters to be used character set
	 * @return a {@code CharacterFilter} containing the specified character set
	 */
	public static CharacterFilter of(String characters) {
		BitSet characterSet = new BitSet();
		for (int i = 0; i < characters.length(); i++) {
			characterSet.set(characters.charAt(i));
		}
		return new CharacterFilter(characterSet);
	}

	/**
	 * @return {@code true} if all characters are element of the character set
	 *         otherwise {@code false}
	 */
	@Override
	public boolean test(String text) {
		for (int i = 0; i < text.length(); i++) {
			if (!this.characters.get(text.charAt(i))) {
				return false;
			}
		}
		return true;
	}
}
