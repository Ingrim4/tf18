package uni.leipzig.textmining.filter;

import java.util.BitSet;
import java.util.function.Predicate;

/**
 * Predicate for testing string's first character to match a set of characters
 * 
 * <p>
 * A string will only pass when the first of its characters is in the given set
 * </p>
 */
public class StartFilter implements Predicate<String> {

	private final BitSet characters;

	/**
	 * Constructs a new StartFilter
	 * 
	 * @param characters allowed set of first characters
	 */
	public StartFilter(BitSet characters) {
		this.characters = characters;
	}

	/**
	 * Returns a {@code StartFilter} with arbitrary long character set
	 * @param characters to be used character set
	 * @return a {@code StartFilter} containing the specified character set
	 */
	public static StartFilter of(String characters) {
		BitSet characterSet = new BitSet();
		for (int i = 0; i < characters.length(); i++) {
			characterSet.set(characters.charAt(i));
		}
		return new StartFilter(characterSet);
	}

	/**
	 * @return {@code true} if the first character is element of the character set
	 *         otherwise {@code false}
	 */
	@Override
	public boolean test(String text) {
		if (text.length() < 1) {
			return true;
		}
		return this.characters.get(text.charAt(0));
	}
}
