package uni.leipzig.textmining.filter;

import uni.leipzig.textmining.config.Config;
import uni.leipzig.textmining.util.statistics.TableStatistics;

public class SentenceFilter {

	private final LengthFilter lengthFilter;
	private final RepeatFilter repeatFilter;
	private final StartFilter startFilter;
	private final CharacterFilter characterFilter;

	public SentenceFilter(Config.Filter filterConfig) {
		this.lengthFilter = new LengthFilter(filterConfig.length, Integer.MAX_VALUE);
		this.repeatFilter = new RepeatFilter(filterConfig.repeat);
		this.startFilter = StartFilter.of(filterConfig.start);
		this.characterFilter = CharacterFilter.of(filterConfig.characters);
	}

	public boolean test(String text, TableStatistics tableStats) {
		if (!this.lengthFilter.test(text)) {
			tableStats.incrementLengthFilterCount();
			return false;
		}

		if (!this.repeatFilter.test(text)) {
			tableStats.incrementRepeatFilterCount();
			return false;
		}

		if (!this.startFilter.test(text)) {
			tableStats.incrementStartFilterCount();
			return false;
		}

		if (!this.characterFilter.test(text)) {
			tableStats.incrementCharacterFilterCount();
			return false;
		}

		return true;
	}
}
