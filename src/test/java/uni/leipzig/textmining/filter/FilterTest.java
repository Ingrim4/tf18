package uni.leipzig.textmining.filter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FilterTest {

	@Test
	public void testLengthFilter() {
		LengthFilter lengthFilter = new LengthFilter(3, 6);

		assertEquals(false, lengthFilter.test(""));
		assertEquals(true,  lengthFilter.test("foo"));
		assertEquals(false,  lengthFilter.test("foo-bar"));
	}

	@Test
	public void testRepeatFilter() {
		RepeatFilter filter = new RepeatFilter(5);
		assertEquals(true,  filter.test(""));
		assertEquals(true,  filter.test("aaa"));
		assertEquals(false, filter.test("aaaaa"));
		assertEquals(true,  filter.test("aaaba"));
	}

	@Test
	public void testCharacterFilter() {
		CharacterFilter filter = CharacterFilter.of("abc");
		assertEquals(true,  filter.test(""));
		assertEquals(true,  filter.test("aaa"));
		assertEquals(false, filter.test("abe"));
		assertEquals(false,  filter.test("e"));
	}

	@Test
	public void testStartFilter() {
		StartFilter filter = StartFilter.of("abc");
		assertEquals(true,  filter.test(""));
		assertEquals(true,  filter.test("aaa"));
		assertEquals(false, filter.test("012"));
		assertEquals(false,  filter.test("e"));
	}
}
