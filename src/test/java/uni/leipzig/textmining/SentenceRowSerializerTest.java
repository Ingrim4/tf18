package uni.leipzig.textmining;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.ByteBuffer;
import java.util.Arrays;

import org.junit.Test;

import uni.leipzig.textmining.sentence.SentenceRow;
import uni.leipzig.textmining.sentence.SentenceRowSerializer;

public class SentenceRowSerializerTest {

	private byte[] array(int value) {
		byte[] array = new byte[16];
		Arrays.fill(array, (byte) value);
		return array;
	}

	@Test
	public void testSerializer() {
		SentenceRowSerializer serializer = SentenceRowSerializer.INSTANCE;
		ByteBuffer byteBuffer = ByteBuffer.allocate(44);

		serializer.write(byteBuffer, new SentenceRow(0, 0, array(42)));
		serializer.write(byteBuffer, new SentenceRow(0, 1, array(13)));

		assertTrue(Arrays.equals(byteBuffer.array(),
				new byte[] { 0, 0, 0, 0, 0, 0, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 42, 0, 0, 0,
						0, 0, 1, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13 }));

		byteBuffer.position(0);

		SentenceRow a = serializer.read(byteBuffer);
		assertEquals(a.databaseId, 0);
		assertEquals(a.sentenceId, 0);
		assertTrue(Arrays.equals(a.hash, array(42)));

		SentenceRow b = serializer.read(byteBuffer);
		assertEquals(b.databaseId, 0);
		assertEquals(b.sentenceId, 1);
		assertTrue(Arrays.equals(b.hash, array(13)));

		assertEquals(a.compareTo(b), 29);
	}
}
