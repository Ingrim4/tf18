package uni.leipzig.textmining;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import uni.leipzig.textmining.sentence.SentenceNormalizer;

public class SentenceNormalizerTest {

	@Test
	public void testNormalizeSentence() {
		SentenceNormalizer serializer = new SentenceNormalizer("\"�");
		assertEquals("00.00.0000 foo bar", serializer.normalize("12.04.1999 foo bar"));
		assertEquals("foo 0000000000 bar", serializer.normalize("foo 1234567890 bar"));
		assertEquals("bar foo", serializer.normalize("bar foo"));
		assertEquals("bar foo", serializer.normalize("bar\" foo�"));
	}
}
