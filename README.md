# tf18 - Riesenkorpus DEU

This was part of the text-mining internship. The task consisted of filtering 7 billion German sentences in over 80 databases and deleting duplicates.

This was achieved by first selecting, filtering and hashing all sentences. The filtered sentences were marked for deletion and the others got sorted by their hash and got wrote to disk (hash inclusive). This happened by default in groups of 4,000,000 sentences per run (a run is sorted output file). The next step was to multi-way-merge all the runs. While merging all duplicates got marked for deletion. This step had to be repeated until only one run file remained. The last step was to delete all maked sentences.

## Some facts and numbers

The select (first) step was blazing fast with around 1.1µs per sentence and only took 8100s for the whole 7 billion sentences. Almost 4 billion sentences got deleted 85% were duplicates. The last run file had a size of about 140GB.

### Config

```json
{
  "database": {                   
    "hostDescription": "127.0.0.1",   Database's hostname (address + port)
    "username": "username",           User to connect with
    "password": "password",           User's password
    "batchSize": 1000                 Size of a sql batch request (select/delete)
  },
  "filter": {
    "length": 3,                      minimum sentence length (inclusive)
    "repeat": 5,                      minimum of chars at a sentence beginning to not be equal
    "start": "<placeholder>",         set of allowed chars at a sentence beginning
    "characters": "<placeholder>"     set of allowed chars for a whole sentence
  },
  "select": {
    "ignoreCharset": "„”‚‘«»‹›",      set of chars to be removed before hashing a sentence
    "elementsPerRuns": 4000000        number of sentences inside a run before being flushed to disk
  },
  "merge": {
    "runsPerMerge": 32,               number of runs to get merge per merge step
    "heapSizeMerge": 1073741824       maximum heap size (in bytes) used while merging
                                      (used to determine buffer sizes)
  },
  "tables": [
    "`deu_news_2012`.`sentences`",    list of all tables to be processed
  ]
}
```

### Commands
- **select**: Starts the select-step which goes through all databases, filters, hashes and writes valid (select-step always ends up in layer 0) and invalid (file per database inside delete-layer -1) entries on the disk. Also creates a new base path (the current date as an ISO string) which is used for future steps (merge, delete).
- **merge &lt;base&gt;**: Starts a merge-step and merges all runs. The base argument should be the base path created from the select step. This step is not recursive and must be executed repeatedly until there is only one run file in the top layer.
- **delete &lt;base&gt;**: Starts the delete-step and deletes all entries that got marked for deletion in the previous steps. Here, too, the base argument should be the base path.
